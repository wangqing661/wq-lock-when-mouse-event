﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WqLockWhenMouseEvent
{
    public partial class Form1 : Form
    {
        MouseHook mh;
        bool LeftTag = false;
        bool RightTag = false;
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 0);

        public Form1()
        {
            InitializeComponent();
            this.Load += Form1_Load;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mh = new MouseHook();
            mh.SetHook();
            mh.MouseDownEvent += mh_MouseDownEvent;
            mh.MouseUpEvent += mh_MouseUpEvent;
        }
        //按下鼠标键触发的事件
        private void mh_MouseDownEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                LeftTag = true;
                richTextBox1.AppendText("按下了左键\n");
                Win32Api.LockWorkStation();
                Application.Exit();
            }
            if (e.Button == MouseButtons.Right)
            {
                RightTag = true;
                richTextBox1.AppendText("按下了右键\n");
                Win32Api.LockWorkStation();
                Application.Exit();
            }
            p1 = e.Location;
        }
        //松开鼠标键触发的事件
        private void mh_MouseUpEvent(object sender, MouseEventArgs e)
        {
            p2 = e.Location;
            double value = Math.Sqrt(Math.Abs(p1.X - p2.X) * Math.Abs(p1.X - p2.X) + Math.Abs(p1.Y - p2.Y) * Math.Abs(p1.Y - p2.Y));
            //if (LeftTag && RightTag && value > 100)
            //{
            //    MessageBox.Show("ok");
            //}
            if (e.Button == MouseButtons.Left)
            {
                richTextBox1.AppendText("松开了左键\n");
            }
            if (e.Button == MouseButtons.Right)
            {
                richTextBox1.AppendText("松开了右键\n");
            }
            richTextBox1.AppendText("移动了" + value + "距离\n");
            RightTag = false;
            LeftTag = false;
            p1 = new Point(0, 0);
            p2 = new Point(0, 0);
        }

        private void Form1_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            mh.UnHook();
        }
    }

    }
